import React from 'react';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import App from 'components/App';

const mockStore = configureStore([]);

test('renders learn react link', () => {
  const store = mockStore({});
  render(
    <Provider store={store}>
      <App />
    </Provider>,
  );
  const element = screen.getByText(/oleeka/i);
  expect(element).toBeInTheDocument();
});
