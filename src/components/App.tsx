import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import NotesView from 'views/NotesView';
import NoteAddView from 'views/NoteAddView';
import NoteEditView from 'views/NoteEditView';
import NoteView from 'views/NoteView';
import PhotosView from 'views/PhotosView';
import TagsView from 'views/TagsView';
import StatsView from 'views/StatsView';
import ShareView from 'views/ShareView';
import IndexView from 'views/IndexView';
import LoginView from 'views/LoginView';
import SettingsView from 'views/SettingsView';
import Link from 'components/Link';
import Header from 'components/navigation/Header';
import Dialogs from 'components/Dialogs';
import ThemeProvider from 'components/ThemeProvider';

import 'components/App.scss';

function NotFound() {
  return <h2>Not Found</h2>;
}

function App() {
  return (
    <ThemeProvider>
      <BrowserRouter>
        <Header></Header>
        <Dialogs />

        <Switch>
          <Route path="/@:username/notes/:query?">
            <NotesView />
          </Route>
          <Route exact path="/@:username/note/add">
            <NoteAddView />
          </Route>
          <Route path="/@:username/note/edit/:nid?">
            <NoteEditView />
          </Route>
          <Route path="/@:username/note/:nid?">
            <NoteView />
          </Route>
          <Route path="/@:username/photos/:query?">
            <PhotosView />
          </Route>
          <Route path="/@:username/tags/:query?">
            <TagsView />
          </Route>
          <Route path="/@:username/stats/:query?">
            <StatsView />
          </Route>
          <Route path="/share/">
            <ShareView />
          </Route>
          <Route path="/login/">
            <LoginView />
          </Route>
          <Route path="/settings/:page?">
            <SettingsView />
          </Route>
          <Route exact path="/">
            <IndexView />
          </Route>
          <Route exact path="/add">
            <Link redirect to="/@:username/note/add" />
          </Route>
          <Route path="/">
            <NotFound />
          </Route>
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
