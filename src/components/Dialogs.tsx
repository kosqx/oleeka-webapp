import React from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { makeStyles, createStyles } from '@material-ui/core/styles';

import { useDispatch, useSelector } from 'react-redux';
import { actions, selectors, Dialog as DialogType } from 'logic/redux';

const useStyles = makeStyles((theme) =>
  createStyles({
    dialogTitle: {
      margin: 0,
      paddingRight: theme.spacing(4),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  }),
);

function ModalDialog({ dialog, handleClose }: { dialog: DialogType; handleClose: () => void }) {
  const classes = useStyles();

  return (
    <Dialog
      open={true}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        <Typography variant="h6" className={classes.dialogTitle}>
          {dialog.title}
        </Typography>
        <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">{dialog.text}</DialogContentText>
      </DialogContent>
      <DialogActions>
        {dialog.buttons.map((button) => (
          <Button
            onClick={() => {
              if (button.onClick) button.onClick();
              handleClose();
            }}
            color={button.color}
            variant={button.variant}
            autoFocus={button.autoFocus}
          >
            {button.text}
          </Button>
        ))}
      </DialogActions>
    </Dialog>
  );
}

export default function Dialogs() {
  const dispatch = useDispatch();
  const dialogs = useSelector(selectors.getDialogs);

  const handleClose = () => {
    dispatch(actions.hideDialog());
  };

  return dialogs?.length > 0 ? <ModalDialog dialog={dialogs[0]} handleClose={handleClose} /> : null;
}
