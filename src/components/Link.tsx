import React from 'react';
import { Link as RouterLink, Redirect, useParams, useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { selectors } from 'logic/redux';

interface PropsKeys {
  to?: string;
  note?: string;
  noteEdit?: string;
  query?: string;
  username?: string;
}

interface LinkProps extends PropsKeys {
  redirect?: boolean;
  // TODO: handle more props passed from ListItem
  className?: string;
}

type ParamsType = { [key: string]: string };

function resolveUsername(props: PropsKeys, params: ParamsType, currentUser: string | null): string {
  if (props.username !== undefined) {
    return props.username;
  } else if (params.username !== undefined) {
    return params.username;
  } else {
    const match = window.location.pathname.match(/\/@([\w.-]+)\//);
    return match === null ? currentUser ?? '' : match[1];
  }
}

export function resolveQueryOrNull(): string | null {
  /**
   * '/@a/notes/'.match() => match[1] === ''
   * '/@a/notes'.match() => match[1] === undefined
   */
  const match = window.location.pathname.match(/\/@[\w.-]+\/(?:notes|photos|tags)(?:\/(.*))?$/);
  return match === null ? null : decodeURIComponent(match[1] || '');
}

function resolveQuery(): string {
  const match = window.location.pathname.match(/\/@[\w.-]+\/(?:notes|photos|tags)\/(.*)$/);
  return match === null ? '' : decodeURIComponent(match[1]);
}

function resolveParams(target: string, params: ParamsType): string {
  for (const [key, value] of Object.entries(params)) {
    target = target.replace(new RegExp(`:${key}\\b`), value);
  }
  return target;
}

function resolveLink(props: PropsKeys, params: ParamsType, currentUser: string | null): string {
  const username = resolveUsername(props, params, currentUser);
  const query = resolveQuery();

  if (props.to !== undefined) {
    return resolveParams(props.to, { username, query, ...params });
  } else if (props.note !== undefined) {
    return `/@${username}/note/${props.note}`;
  } else if (props.noteEdit !== undefined) {
    return `/@${username}/note/edit/${props.noteEdit}`;
  } else if (props.query !== undefined) {
    return `/@${username}/notes/${props.query}`;
  } else {
    return `/@${username}/notes/`;
  }
}

export function useRedirect(): (props: PropsKeys) => void {
  const params = useParams<ParamsType>();
  const currentUser = useSelector(selectors.getUsername);
  const history = useHistory();
  return (props: PropsKeys) => {
    const link = resolveLink(props, params, currentUser);
    history.push(link);
  };
}

const Link: React.FC<LinkProps> = (props) => {
  const params = useParams<ParamsType>();
  const currentUser = useSelector(selectors.getUsername);
  const link = resolveLink(props, params, currentUser);

  if (props.redirect) {
    return <Redirect to={link}></Redirect>;
  } else {
    return (
      <RouterLink to={link} className={props.className}>
        {props.children}
      </RouterLink>
    );
  }
};

export default Link;
