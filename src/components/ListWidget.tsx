import React from 'react';

import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import Link from 'components/Link';

interface ListNode {
  id: string;
  primaryText: string;
  secondaryText?: string;
  icon: React.ComponentType;
  linkTo: string;
}

interface ListSection {
  id: string;
  items: ListNode[];
}

export type ListSections = ListSection[];

interface ListWidgetProps {
  selectedItem: string;
  sections: ListSection[];
}

export default function ListWidget(props: ListWidgetProps) {
  return (
    <>
      {props.sections.map((section, sectionIndex) => (
        <React.Fragment key={section.id}>
          <List>
            {section.items.map((item, itemIndex) => (
              <ListItem
                key={item.id}
                button
                component={Link}
                to={item.linkTo}
                selected={props.selectedItem === item.id}
              >
                <ListItemIcon>
                  <item.icon />
                </ListItemIcon>
                <ListItemText primary={item.primaryText} secondary={item.secondaryText} />
              </ListItem>
            ))}
          </List>
          {sectionIndex + 1 < props.sections.length ? <Divider /> : null}
        </React.Fragment>
      ))}
    </>
  );
}
