import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      '& ul': {
        justifyContent: 'center',
      },
    },
  }),
);

interface PaginationParams {
  skip: number;
  limit: number;
}

export function calculatePaginationParams(page: number, perPage: number): PaginationParams {
  return { skip: page * perPage, limit: perPage };
}

interface PaginationWidgetProps {
  page: number;
  parPage: number;
  total?: number;
  onChange: (page: number) => void;
}

export default function PaginationWidget(props: PaginationWidgetProps) {
  const classes = useStyles();
  if (typeof props.total === 'undefined') {
    return <></>;
  }
  const count = Math.floor((props.total - 1) / props.parPage) + 1;
  return (
    <div className={classes.root}>
      <Pagination
        page={props.page + 1}
        count={count}
        onChange={(event: object, newPage: number) => props.onChange(newPage - 1)}
        variant="outlined"
        siblingCount={1}
      />
      {/* <div>{props.total}</div> */}
    </div>
  );
}
