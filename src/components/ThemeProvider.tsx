import React from 'react';
import { useSelector } from 'react-redux';

import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import { selectors } from 'logic/redux';

const ThemeProvider: React.FC<{}> = (props) => {
  const themeSetting = useSelector(selectors.getTheme);
  const theme = createMuiTheme({
    palette: {
      type: themeSetting,
    },
  });

  return (
    <div className="App">
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        {props.children}
      </MuiThemeProvider>
    </div>
  );
};

export default ThemeProvider;
