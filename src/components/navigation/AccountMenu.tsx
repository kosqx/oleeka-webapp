import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import Link, { useRedirect } from 'components/Link';
import { actions, selectors } from 'logic/redux';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    menu: {
      '& a': {
        color: 'inherit',
        width: '100%',
      },
    },
    divider: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  }),
);

interface AccountMenuProps {
  anchorElement: null | HTMLElement;
  onClose: () => void;
}

export default function AccountMenu(props: AccountMenuProps) {
  const dispatch = useDispatch();
  const currentUser = useSelector(selectors.getUsername);
  const additionalAccounts = useSelector(selectors.getAdditionalAccounts);
  const classes = useStyles();
  const redirect = useRedirect();

  const handleMenuClose = () => {
    props.onClose();
  };

  const handleToggleTheme = () => {
    props.onClose();
    dispatch(actions.toggleTheme());
  };

  const handleLogout = () => {
    props.onClose();
    dispatch(actions.logout());
    redirect({ to: '/login' });
  };

  const handleAccountSwitch = (username: string) => {
    console.log(username);
    props.onClose();
    dispatch(actions.switchAccount(username));
    redirect({ username: username, query: '' });
  };

  const additionalAccountsItems = additionalAccounts.map((account) => (
    <MenuItem key={account.username} onClick={() => handleAccountSwitch(account.username)}>
      {account.username}
    </MenuItem>
  ));

  return (
    <Menu
      id="header-account-menu"
      className={classes.menu}
      anchorEl={props.anchorElement}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      keepMounted
      open={Boolean(props.anchorElement)}
      onClose={handleMenuClose}
    >
      {currentUser === null
        ? [
            <MenuItem key="theme" onClick={handleToggleTheme}>
              Toggle Theme
            </MenuItem>,
            additionalAccounts.length > 0 ? (
              <Divider key="divider-all-accounts" className={classes.divider} />
            ) : null,
            ...additionalAccountsItems,
            <Divider key="divider" className={classes.divider} />,
            <MenuItem key="login" onClick={handleMenuClose}>
              <Link to="/login">Login</Link>
            </MenuItem>,
          ]
        : [
            <MenuItem key="info" disabled>
              <div>
                <strong>@{currentUser}</strong>
                <div>{currentUser}@email.com</div>
              </div>
            </MenuItem>,
            <Divider key="divider-all-accounts" className={classes.divider} />,
            ...additionalAccountsItems,
            <MenuItem key="login" onClick={handleMenuClose}>
              <Link to="/login">Add another account</Link>
            </MenuItem>,
            <Divider key="divider1" className={classes.divider} />,
            <MenuItem key="settings" onClick={handleMenuClose}>
              <Link to="/settings/">Settings</Link>
            </MenuItem>,
            <MenuItem key="theme" onClick={handleToggleTheme}>
              Toggle Theme
            </MenuItem>,
            <Divider key="divider2" className={classes.divider} />,
            <MenuItem key="logout" onClick={handleLogout}>
              Logout
            </MenuItem>,
          ]}
    </Menu>
  );
}
