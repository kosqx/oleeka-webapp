import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import clsx from 'clsx';

import { fade, makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Fade from '@material-ui/core/Fade';

import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';

import AccountMenu from 'components/navigation/AccountMenu';
import Sidebar from 'components/navigation/Sidebar';
import Link, { useRedirect, resolveQueryOrNull } from 'components/Link';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbarMargin: theme.mixins.toolbar,
    toolbarHasFocus: {
      [theme.breakpoints.down('xs')]: {
        '& h6': {
          display: 'none',
        },
        '& $search': {
          marginLeft: theme.spacing(0),
        },
      },
      [theme.breakpoints.up('sm')]: {
        '& $search': {
          width: '100%',
        },
        '& $inputInput': {
          width: '100% !important',
        },
      },
    },
    menuButton: {
      marginRight: theme.spacing(1),
    },
    title: {
      flexGrow: 1,
      '& a': {
        color: 'white',
      },
    },
    addButton: {
      '& a': {
        color: 'white',
        height: 24,
        fontSize: '1.0rem',
      },
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: theme.spacing(2),
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 330,
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 1),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
      width: '100%',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      paddingLeft: `calc(1em + ${theme.spacing(3)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
    },
  }),
);

export default function Header() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [sidebarOpen, setSidebarOpen] = useState<boolean>(false);
  const [scroll, setScroll] = useState<{ active: boolean; top: number }>({ active: false, top: 0 });
  const [searchFocus, setSearchFocus] = useState<boolean>(false);
  const [queryText, setQueryText] = useState<string>('');
  const location = useLocation();
  const redirect = useRedirect();

  useEffect(() => {
    const handleScroll = (event: any) => {
      const top = event.target.documentElement.scrollTop;
      setScroll({ active: top > scroll.top, top: top });
    };

    window.addEventListener('scroll', handleScroll);

    return function cleanup() {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [scroll.top]);

  useEffect(() => {
    const resolvedQuery = resolveQueryOrNull();
    if (typeof resolvedQuery === 'string') {
      setQueryText(resolvedQuery);
    }
  }, [location.pathname]);

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  return (
    <div>
      <Fade in={!scroll.active}>
        <AppBar position="fixed">
          <Toolbar className={clsx(searchFocus && classes.toolbarHasFocus)}>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
              onClick={() => setSidebarOpen(true)}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              <Link>Oleeka</Link>
            </Typography>
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{
                  'aria-label': 'search',
                  autoComplete: 'off',
                  autoCapitalize: 'off',
                  autoCorrect: 'off',
                }}
                value={queryText}
                onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                  setQueryText(event.target.value)
                }
                onKeyDown={(event: any) => {
                  if (event.keyCode === 9 || event.keyCode === 13) {
                    redirect({ query: queryText });
                  }
                }}
                onFocus={() => setSearchFocus(true)}
                onBlur={() => setSearchFocus(false)}
              />
            </div>
            <IconButton aria-label="add new note" className={classes.addButton}>
              <Link to="/@:username/note/add">
                <AddCircleIcon />
              </Link>
            </IconButton>
            <IconButton
              aria-label="user menu"
              aria-controls="header-account-menu"
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircleIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
      </Fade>

      <AccountMenu anchorElement={anchorEl} onClose={() => setAnchorEl(null)} />
      <Sidebar open={sidebarOpen} onClose={() => setSidebarOpen(false)} />
      <div className={classes.toolbarMargin} />
    </div>
  );
}
