import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';

import ImageIcon from '@material-ui/icons/Image';
import ListAltIcon from '@material-ui/icons/ListAlt';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import MailIcon from '@material-ui/icons/Mail';

import ListWidget, { ListSections } from 'components/ListWidget';

const useStyles = makeStyles({
  list: {
    width: 280,
  },
});

interface SidebarProps {
  open: boolean;
  onClose: () => void;
}

export default function Sidebar(props: SidebarProps) {
  const classes = useStyles();

  const closeSidebar = (event: React.KeyboardEvent | React.MouseEvent) => {
    if (
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }

    props.onClose();
  };

  const sections: ListSections = [
    {
      id: 'related',
      items: [
        {
          id: 'notes',
          primaryText: 'Notes',
          icon: ListAltIcon,
          linkTo: '/@:username/notes/:query',
        },
        {
          id: 'photos',
          primaryText: 'Photos',
          icon: ImageIcon,
          linkTo: '/@:username/photos/:query',
        },
        {
          id: 'tags',
          primaryText: 'Tags',
          icon: LocalOfferIcon,
          linkTo: '/@:username/tags/:query',
        },
        {
          id: 'stats',
          primaryText: 'Stats',
          icon: LocalOfferIcon,
          linkTo: '/@:username/stats/:query',
        },
      ],
    },
    {
      id: 'filler',
      items: [
        {
          id: 'foo',
          primaryText: 'Foo',
          icon: MailIcon,
          linkTo: '/',
        },
      ],
    },
  ];

  return (
    <Drawer anchor="left" open={props.open} onClose={closeSidebar}>
      <div
        className={classes.list}
        role="presentation"
        onClick={closeSidebar}
        onKeyDown={closeSidebar}
      >
        <ListWidget selectedItem={''} sections={sections} />;
      </div>
    </Drawer>
  );
}
