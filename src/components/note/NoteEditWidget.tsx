import React from 'react';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

import { NoteInputType } from 'data/models';

const INPUT_PROPS = {
  autoComplete: 'off',
  autoCapitalize: 'off',
  autoCorrect: 'off',
};

interface NoteEditWidgetProps {
  note: NoteInputType;
  onChange: (note: NoteInputType) => void;
  onSubmit: () => void;
}

function NoteEditWidget(props: NoteEditWidgetProps) {
  const handleAttrChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    props.onChange({
      ...props.note,
      attrs: { ...props.note.attrs, [event.target.name]: event.target.value },
    });

  const attrsInputs = Object.entries(props.note.attrs);

  return (
    <div className="note-widget">
      <Grid container spacing={3}>
        {attrsInputs.map(([name, value]) => (
          <Grid item xs={12}>
            <TextField
              id={`input-attr-${name}`}
              name={name}
              label={name}
              value={value}
              onChange={handleAttrChange}
              variant="outlined"
              fullWidth
              inputProps={INPUT_PROPS}
            />
          </Grid>
        ))}
        <Grid item xs={12}>
          <TextField
            id="input-text"
            label="Text"
            value={props.note.text}
            onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) =>
              props.onChange({ ...props.note, text: event.target.value })
            }
            variant="outlined"
            multiline
            rows={3}
            rowsMax={10}
            fullWidth
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="input-tags"
            label="Tags"
            value={props.note.input}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              props.onChange({ ...props.note, input: event.target.value })
            }
            variant="outlined"
            fullWidth
            inputProps={INPUT_PROPS}
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            variant="contained"
            color="primary"
            fullWidth
            onClick={(event) => props.onSubmit()}
          >
            Save
          </Button>
        </Grid>
      </Grid>
    </div>
  );
}

export default NoteEditWidget;
