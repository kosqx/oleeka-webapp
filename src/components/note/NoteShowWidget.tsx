import React from 'react';

import Link from 'components/Link';
import { NoteAttrsType, NoteFileType, NoteType } from 'data/models';
import { COLORS_MAPPING } from 'logic/color';
import { formatDateTime, formatFileSize } from 'logic/format';

import { useDispatch } from 'react-redux';
import { actions } from 'logic/redux';
import { deleteNote } from 'data';

import './NoteShowWidget.scss';

function NoteTitle(props: { attrs: NoteAttrsType }) {
  if (props.attrs.href) {
    return (
      <h3 className="note-title">
        <a href={props.attrs.href}>{props.attrs.title}</a>
      </h3>
    );
  } else if (props.attrs.title) {
    return <h3 className="note-title">{props.attrs.title}</h3>;
  } else {
    return null;
  }
}

function NoteLink(props: { attrs: NoteAttrsType }) {
  const { href } = props.attrs;
  if (href) {
    return (
      <div>
        <a href={href}>{href}</a>
      </div>
    );
  } else {
    return null;
  }
}

function NoteStatus(props: { attrs: NoteAttrsType; nid: string | null; onDelete: () => void }) {
  const dispatch = useDispatch();

  const rating = +(props.attrs.rating || 0);
  const stars = [1, 2, 3].map((index) => (index <= rating ? '\u2605' : '\u2606'));
  const created = formatDateTime(props.attrs.created);
  const modified = formatDateTime(props.attrs.modified);

  const handleDeleteClick = () => {
    dispatch(
      actions.showDialog({
        title: 'Do you want to delete this note?',
        text: 'This operation cannot be undone.',
        buttons: [
          { text: 'Yes, delete', color: 'secondary', onClick: props.onDelete },
          { text: 'Cancel', color: 'primary', autoFocus: true },
        ],
      }),
    );
  };

  return (
    <div className="note-status">
      <span className="note-status-rating">{stars}</span>
      <span className="note-status-created" title={props.attrs.created}>
        {created}
      </span>
      <span className="note-status-modified" title={props.attrs.modified}>
        {modified}
      </span>
      <Link note={props.nid || undefined} className="note-status-show">
        show
      </Link>
      <Link noteEdit={props.nid || undefined} className="note-status-edit">
        edit
      </Link>
      <span className="note-status-delete" onClick={handleDeleteClick}>
        delete
      </span>
    </div>
  );
}

function NoteTags(props: { tags: string[] }) {
  return (
    <div className="note-tags">
      {props.tags.map((tag) => (
        <span key={tag}>
          <Link query={tag}>{tag}</Link>
        </span>
      ))}
    </div>
  );
}

function NoteAttrs(props: { attrs: NoteAttrsType }) {
  const { created, modified, rating, href, title, ...attrs } = props.attrs;
  return <div className="note-attrs">{JSON.stringify(attrs, null, 1)}</div>;
}

function NoteText(props: { text: string }) {
  if (props.text) {
    return <pre className="note-text">{props.text}</pre>;
  } else {
    return null;
  }
}

function NoteFiles(props: { files: NoteFileType[] }) {
  if (props.files.length === 0) {
    return null;
  }
  return (
    <table className="note-files">
      <thead>
        <tr>
          <th>Name</th>
          <th>Mime</th>
          <th>Size</th>
        </tr>
      </thead>
      <tbody>
        {props.files.map((file) => (
          <tr key={file.fid} className="note-file">
            <td>{file.name}</td>
            <td>{file.mime}</td>
            <td>{formatFileSize(file.size)}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

function NoteBox(props: React.PropsWithChildren<{ note: NoteType }>) {
  const color = props.note?.attrs?.color;
  if (color !== null && COLORS_MAPPING.hasOwnProperty(color)) {
    const namedColor = COLORS_MAPPING[color];
    const [backgroundColor, borderColor] = true
      ? [namedColor.light, namedColor.dark]
      : [namedColor.dark, namedColor.light];
    const style = {
      backgroundColor: backgroundColor,
      border: `1px solid ${borderColor}`,
      borderRadius: 5,
    };
    return (
      <div className="note-widget" style={style}>
        {props.children}
      </div>
    );
  } else {
    return <div className="note-widget">{props.children}</div>;
  }
}

function NoteShowWidget(props: { note: NoteType }) {
  const note = props.note;

  const handleDeleteClick = async () => {
    if (!note.nid) return;
    await deleteNote('', note.nid);
  };

  return (
    <NoteBox note={note}>
      <NoteTitle attrs={note.attrs} />
      <NoteLink attrs={note.attrs} />
      <NoteStatus attrs={note.attrs} nid={note.nid} onDelete={handleDeleteClick} />
      <NoteTags tags={note.tags} />
      <NoteAttrs attrs={note.attrs} />
      <NoteText text={note.text} />
      <NoteFiles files={note.files} />
    </NoteBox>
  );
}

export default NoteShowWidget;
