import React from 'react';
import preval from 'preval.macro';

import Typography from '@material-ui/core/Typography';

import { setDocumentTitle } from 'logic/page';

const BUILD_INFO = preval`
const { execSync } = require('child_process')
const commit = execSync('git rev-parse HEAD', {encoding: 'utf-8'})
const date = new Date().toISOString();
module.exports = { commit, date }
`;

export default function AboutPage() {
  setDocumentTitle('About', 'settings');

  return (
    <div>
      <Typography variant="h3">About</Typography>
      <Typography variant="body2">
        WebApp Build timetime: {BUILD_INFO.date.substring(0, 19)}
      </Typography>
      <Typography variant="body2">
        WebApp Build commit: {BUILD_INFO.commit.substring(0, 7)}
      </Typography>
    </div>
  );
}
