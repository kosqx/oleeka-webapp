import React, { useEffect, useState } from 'react';

import Typography from '@material-ui/core/Typography';

import { setDocumentTitle } from 'logic/page';

export default function DebugPage() {
  setDocumentTitle('Debug', 'settings');
  const [info, setInfo] = useState<string>('');

  useEffect(() => {
    const fetchDebug = async () => {
      const storage = await navigator.storage.estimate();
      const connection = (window.navigator as any).connection;
      const dict = {
        storage,
        hardwareConcurrency: navigator.hardwareConcurrency,
        userAgent: navigator.userAgent,
        connection: {
          onLine: window.navigator.onLine,
          type: connection.type,
          effectiveType: connection.effectiveType,
          downlink: connection.downlink,
          rtt: connection.rtt,
          saveData: connection.saveData,
        },
      };
      setInfo(JSON.stringify(dict, null, 2));
    };
    fetchDebug();
  }, []);

  return (
    <div>
      <Typography variant="h3">Debug</Typography>
      <pre>{info}</pre>
    </div>
  );
}
