import React from 'react';

import Typography from '@material-ui/core/Typography';

import { setDocumentTitle } from 'logic/page';

export default function ThemePage() {
  setDocumentTitle('Theme', 'settings');

  return (
    <div>
      <Typography variant="h3">Theme</Typography>
    </div>
  );
}
