import { openDB } from 'idb';

import { NoteType } from 'data/models';

interface NoteCacheType extends NoteType {
  fetchTime: Date;
}

function dateToMiliseconds(date: Date): number {
  return (date as unknown) as number;
}

export function noteCacheAge(note: NoteCacheType): number {
  const diff = dateToMiliseconds(new Date()) - dateToMiliseconds(note.fetchTime);
  return diff / 1000.0;
}

async function openCache(username: string) {
  return await openDB(`oleeka-store-${username}`, 1, {
    upgrade(db) {
      db.createObjectStore('notes', { keyPath: 'nid' });
    },
  });
}

export const cacheDatabase = {
  async getNote(username: string, nid: string): Promise<NoteCacheType | undefined> {
    return (await openCache(username)).get('notes', nid);
  },
  async storeNotes(username: string, notes: NoteType[]) {
    const tx = (await openCache(username)).transaction('notes', 'readwrite');
    await Promise.all([
      ...(notes.map((note) => {
        const cacheNote: NoteCacheType = { ...note, fetchTime: new Date() };
        return tx.store.put(cacheNote);
      }) as any),
      tx.done,
    ]);
  },
};
