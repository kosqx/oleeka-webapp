import { storeLogin } from 'logic/accounts';
import { loadSettings } from 'logic/settings';

const BASE_URL = '';

interface FetchRequest {
  method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
  url: string;
  data?: any;
}
interface FetchRequestRaw extends FetchRequest {
  bearer?: string;
}
interface FetchResponse {
  status: number;
  json: any;
}

export const apiFetchRaw = async (request: FetchRequestRaw): Promise<FetchResponse> => {
  const { url, data, bearer, ...rest } = request;
  const resource = url.includes('://') ? url : `${BASE_URL}${url}`;

  const init = {
    headers: {
      ...(bearer ? { Authorization: `Bearer ${bearer}` } : ({} as any)),
      ...(data !== undefined ? { 'Content-Type': 'application/json' } : {}),
    },
    ...rest,
    ...(data !== undefined ? { body: JSON.stringify(data) } : {}),
  };

  const res = await window.fetch(resource, init);
  return { status: res.status, json: await res.json() };
};

const isTokenExpired = (token: string | null): boolean => {
  if (!token) return true;
  const payload = (token.match(/\.([\w/=+]+)\./) || ['', '{}'])[1];
  const timestampExp = JSON.parse(atob(payload))['exp'];
  const timestampNow = new Date().getTime() / 1000.0;
  return timestampExp - timestampNow < 5;
};

const refreshToken = async (username: string): Promise<string> => {
  const res = await apiFetchRaw({
    method: 'POST',
    url: `/api/v2/auth/refresh/${username}`,
  });
  const accessToken = res.json.access_token;
  storeLogin({ username, accessToken });
  return accessToken;
};

export const apiFetch = async (request: FetchRequest): Promise<FetchResponse> => {
  /// FIXME: add error handling
  let { username, accessToken } = loadSettings('account', { username: '-', accessToken: '-' });
  let tokenRefreshed = false;

  if (isTokenExpired(accessToken)) {
    accessToken = await refreshToken(username);
    tokenRefreshed = true;
  }

  const res = await apiFetchRaw({ ...request, bearer: accessToken });

  if (res.status < 300) {
    return res;
  }
  if (!tokenRefreshed) {
    accessToken = await refreshToken(username);
  }

  return await apiFetchRaw({ ...request, bearer: accessToken });
};
