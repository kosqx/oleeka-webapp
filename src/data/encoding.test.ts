import { buildGetNotesParams, buildGetRelatedParams, decodeNote } from 'data/encoding';
import { Nested } from 'data/queryTerms';

test.each([
  [{}, ''],
  [{ username: 'foo' }, '?username=foo'],
  [{ query: new Nested('foo bar') }, '?query=foo%20bar'],
  [{ random: false }, '?random=no'],
  [{ random: true }, '?random=yes'],
  [{ skip: 100 }, '?skip=100'],
  [{ limit: 20 }, '?limit=20'],
  [{ skip: 100, limit: 20 }, '?skip=100&limit=20'],
  [{ fetchTotal: false }, '?fetch_total=no'],
  [{ fetchTotal: true }, '?fetch_total=yes'],
  [{ selectFields: 'text,tags' }, '?select_fields=text%2Ctags'],
])('buildGetNotesParams(%j, %j)', (params, expected) => {
  expect(buildGetNotesParams(params)).toBe(expected);
});

test.each([
  [{}, ''],
  [{ username: 'foo' }, '?username=foo'],
  [{ query: new Nested('foo bar') }, '?query=foo%20bar'],
  [{ fetchTotal: false }, '?fetch_total=no'],
  [{ fetchTotal: true }, '?fetch_total=yes'],
  [{ fetchTags: false }, '?fetch_tags=no'],
  [{ fetchTags: true }, '?fetch_tags=yes'],
  [{ fetchAttrs: false }, '?fetch_attrs=no'],
  [{ fetchAttrs: true }, '?fetch_attrs=yes'],
  [{ query: new Nested('foo bar'), fetchTags: true }, '?query=foo%20bar&fetch_tags=yes'],
])('buildGetRelatedParams(%j, %j)', (params, expected) => {
  expect(buildGetRelatedParams(params)).toBe(expected);
});

test.each([
  [
    {},
    {
      nid: null,
      text: '',
      tags: [],
      attrs: {},
      files: [],
    },
  ],
  [
    {
      nid: '20220101T123456R000000',
      text: ['abc\n', 'def'],
      tags: ['foo'],
      attrs: { abc: 'def' },
      files: [{ fid: 'F123' }],
      something: 123,
    },
    {
      nid: '20220101T123456R000000',
      text: 'abc\ndef',
      tags: ['foo'],
      attrs: { abc: 'def' },
      files: [{ fid: 'F123' }],
    },
  ],
])('decodeNote(%j, %j)', (params, expected) => {
  expect(decodeNote(params)).toEqual(expected);
});
