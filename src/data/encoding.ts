import { GetNotesParams, GetRelatedParams, NoteType } from 'data/models';

function encodeBool(value: boolean): string {
  return value ? 'yes' : 'no';
}

export function buildGetNotesParams(params: GetNotesParams): string {
  const result = [];

  if (params.username !== undefined) {
    result.push(`username=${encodeURIComponent(params.username)}`);
  }
  if (params.query !== undefined) {
    result.push(`query=${encodeURIComponent(params.query.format())}`);
  }
  if (params.random !== undefined) {
    result.push(`random=${encodeBool(params.random)}`);
  }
  if (params.skip !== undefined) {
    result.push(`skip=${params.skip}`);
  }
  if (params.limit !== undefined) {
    result.push(`limit=${params.limit}`);
  }
  if (params.selectFields !== undefined) {
    result.push(`select_fields=${encodeURIComponent(params.selectFields)}`);
  }
  if (params.fetchTotal !== undefined) {
    result.push(`fetch_total=${encodeBool(params.fetchTotal)}`);
  }

  return result.length > 0 ? `?${result.join('&')}` : '';
}

export function buildGetRelatedParams(params: GetRelatedParams): string {
  const result = [];

  if (params.username !== undefined) {
    result.push(`username=${encodeURIComponent(params.username)}`);
  }
  if (params.query !== undefined) {
    result.push(`query=${encodeURIComponent(params.query.format())}`);
  }
  if (params.fetchTotal !== undefined) {
    result.push(`fetch_total=${encodeBool(params.fetchTotal)}`);
  }
  if (params.fetchTags !== undefined) {
    result.push(`fetch_tags=${encodeBool(params.fetchTags)}`);
  }
  if (params.fetchAttrs !== undefined) {
    result.push(`fetch_attrs=${encodeBool(params.fetchAttrs)}`);
  }
  if (params.fetchDates !== undefined) {
    result.push(`fetch_dates=${params.fetchDates}`);
  }

  return result.length > 0 ? `?${result.join('&')}` : '';
}

export function decodeNote(note: any): NoteType {
  return {
    nid: note.nid ?? null,
    text: (note.text ?? []).join(''),
    tags: note.tags ?? [],
    attrs: note.attrs ?? {},
    files: note.files ?? [],
  };
}
