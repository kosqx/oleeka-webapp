import {
  Credentials,
  LoginResponse,
  NoteType,
  NoteInputType,
  NotesResponseType,
  GetNotesParams,
  GetRelatedParams,
  RelatedType,
} from 'data/models';
import { cacheDatabase, noteCacheAge } from 'data/cache';
import { apiFetchRaw, apiFetch } from 'data/client';
import { buildGetNotesParams, buildGetRelatedParams, decodeNote } from 'data/encoding';

export const apiLogin = async (credentials: Credentials): Promise<LoginResponse> => {
  const res = await apiFetchRaw({
    method: 'POST',
    url: '/api/v2/auth/login',
    data: {
      username: credentials.username,
      password: credentials.password,
    },
  });
  if (res.status === 200) {
    return { success: true, accessToken: res.json.access_token };
  } else {
    return { success: false, message: res.json.detail };
  }
};

export const getNotes = async (params: GetNotesParams): Promise<NotesResponseType> => {
  const res = await apiFetch({
    method: 'GET',
    url: `/api/v2/notes${buildGetNotesParams(params)}`,
  });

  if (res.status === 200) {
    const notes = res.json.notes.map(decodeNote);
    cacheDatabase.storeNotes(params.username || 'INVALID', notes);
    return {
      notes: notes,
      total: res.json.total,
    };
  } else {
    return {
      notes: [],
      total: 0,
    };
  }
};

export const getNote = async (username: string, nid: string): Promise<NoteType | null> => {
  const note = await cacheDatabase.getNote(username, nid);
  if (typeof note !== 'undefined') {
    if (noteCacheAge(note) < 10) {
      return note;
    }
  }

  const response = await apiFetch({
    method: 'GET',
    url: `/api/v2/notes/${nid}`,
  });
  const noteFetched = decodeNote(response.json.note);
  cacheDatabase.storeNotes(username, [noteFetched]);
  return noteFetched;
};

export const deleteNote = async (username: string, nid: string): Promise<boolean> => {
  // TODO: remove from cache
  // const note = await cacheDatabase.deleteNote(username, nid);

  const response = await apiFetch({
    method: 'DELETE',
    url: `/api/v2/notes/${nid}`,
  });
  return true;
};

export const saveNote = async (username: string, noteInput: NoteInputType): Promise<NoteType> => {
  const data = { note: noteInput };
  const [method, url]: ['POST' | 'PUT', string] = noteInput.nid
    ? ['PUT', `/api/v2/notes/${noteInput.nid}`]
    : ['POST', '/api/v2/notes'];
  const response = await apiFetch({ method, url, data });
  const note = decodeNote(response.json.note);
  cacheDatabase.storeNotes(username, [note]);
  return note;
};

export const getRelated = async (params: GetRelatedParams): Promise<RelatedType> => {
  const res = await apiFetch({
    method: 'GET',
    url: `/api/v2/related${buildGetRelatedParams(params)}`,
  });
  return res.json;
};
