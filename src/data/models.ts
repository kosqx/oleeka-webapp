import { Term } from 'data/queryTerms';

export interface Credentials {
  username: string;
  password: string;
}
export function initCredentials(): Credentials {
  return { username: '', password: '' };
}

export type LoginResponse =
  | { success: true; accessToken: string }
  | { success: false; message: string };

export interface NoteAttrsType {
  [key: string]: string;
}

export type NoteFileEncodingOptions = 'omitted' | 'text' | 'base64';
export interface NoteFileType {
  fid: string;
  name: string;
  hash: string;
  mime: string;
  size: number;
  meta: {};
  encoding?: NoteFileEncodingOptions;
  data: string[];
}

export interface NoteType {
  nid: string | null;
  tags: string[];
  attrs: NoteAttrsType;
  text: string;
  files: NoteFileType[];
}

export function initNoteType(): NoteType {
  return { nid: null, tags: [], attrs: {}, text: '', files: [] };
}

export interface NoteInputType extends NoteType {
  input: string;
}

export function initNoteInputType(): NoteInputType {
  return { ...initNoteType(), input: '' };
}

export interface NotesResponseType {
  notes: NoteType[];
  total?: number;
}

export function initNotesResponseType(): NotesResponseType {
  return { notes: [], total: undefined };
}

export interface GetNotesParams {
  username?: string;
  query?: Term;
  random?: boolean;
  skip?: number;
  limit?: number;
  selectFields?: string;
  fetchTotal?: boolean;
}

export interface GetRelatedParams {
  username?: string;
  query?: Term;
  fetchTotal?: boolean;
  fetchTags?: boolean;
  fetchAttrs?: boolean;
  fetchDates?: 'year' | 'month' | 'day' | 'hour';
}

export interface RelatedType {
  total?: number | null;
  tags_empty?: number | null;
  tags: { [tag: string]: number } | null;
  attrs: { [attr: string]: number } | null;
  dates: { [attr: string]: number } | null;
}

export function initRelatedType(): RelatedType {
  return { total: null, tags_empty: null, tags: null, attrs: null, dates: null };
}
