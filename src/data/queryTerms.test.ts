import * as terms from 'data/queryTerms';

test.each([
  [new terms.Term(), 'NONE', ['none']],
  [new terms.All(), 'ALL', ['all']],
  [new terms.Nid(['123ABC']), 'NID=123ABC', ['nid', ['123ABC']]],
  [new terms.Nid(['123', 'XYZ']), 'NID=123,XYZ', ['nid', ['123', 'XYZ']]],
  [new terms.Tag('foo'), 'foo', ['tag', 'foo', 'exact']],
  [new terms.Tag('foo', 'exact'), 'foo', ['tag', 'foo', 'exact']],
  [new terms.Tag('foo', 'prefix'), 'foo*', ['tag', 'foo', 'prefix']],
  [new terms.Tag('foo', 'suffix'), '*foo', ['tag', 'foo', 'suffix']],
  [new terms.Tag('foo', 'infix'), '*foo*', ['tag', 'foo', 'infix']],
  [new terms.Attr('foo', '?', ''), 'foo?', ['attr', 'foo', '?', '']],
  [terms.Attr.exists('foo'), 'foo?', ['attr', 'foo', '?', '']],
  [new terms.Attr('foo', '=', 'abc'), 'foo="abc"', ['attr', 'foo', '=', 'abc']],
  [new terms.Attr('foo', '*==', 'abc'), 'foo*=="abc"', ['attr', 'foo', '*==', 'abc']],
  [new terms.Text('foo bar'), '"foo bar"', ['text', 'foo bar']],
  [new terms.Text('foo"bar'), '"foo\x5c"bar"', ['text', 'foo"bar']],
  [new terms.Not(new terms.Term()), '!(NONE)', ['not', ['none']]],
  // and
  [new terms.And([]), 'ALL', ['and', []]],
  [new terms.And([0, 1, '', 'x', new terms.All(), [1], {}]), '(ALL)', ['and', [['all']]]],
  [new terms.And([new terms.Tag('foo')]), '(foo)', ['and', [['tag', 'foo', 'exact']]]],
  [
    new terms.And([new terms.Tag('foo'), new terms.All()]),
    '(foo AND ALL)',
    ['and', [['tag', 'foo', 'exact'], ['all']]],
  ],
  // or
  [new terms.Or([]), 'NONE', ['or', []]],
  [new terms.Or([0, 1, '', 'x', new terms.All(), [1], {}]), '(ALL)', ['or', [['all']]]],
  [new terms.Or([new terms.Tag('foo')]), '(foo)', ['or', [['tag', 'foo', 'exact']]]],
  [
    new terms.Or([new terms.Tag('foo'), new terms.All()]),
    '(foo OR ALL)',
    ['or', [['tag', 'foo', 'exact'], ['all']]],
  ],
  // nested
  [new terms.Nested('foo bar'), 'foo bar', ['nested', 'foo bar']],
  [
    new terms.And([new terms.Attr('kind', '===', 'photo'), new terms.Nested('foo bar')]),
    '(kind==="photo" AND foo bar)',
    [
      'and',
      [
        ['attr', 'kind', '===', 'photo'],
        ['nested', 'foo bar'],
      ],
    ],
  ],
  [new terms.And([new terms.All(), new terms.Nested('')]), '(ALL)', ['and', [['all']]]],
  [new terms.And([new terms.All(), new terms.Nested(null)]), '(ALL)', ['and', [['all']]]],
])('%j.format() === %j', (term, expectedQuery, expectedStruct) => {
  expect(term.format()).toBe(expectedQuery);
  expect(term.toStruct()).toEqual(expectedStruct);
});
