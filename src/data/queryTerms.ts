type StructType = string[] | (string | StructType)[];

function escapeString(value: string): string {
  return `"${value.replace('"', '\x5c"')}"`;
}

export class Term {
  format() {
    return 'NONE';
  }
  toStruct(): StructType {
    return ['none'];
  }
}

export class All extends Term {
  format() {
    return 'ALL';
  }
  toStruct(): StructType {
    return ['all'];
  }
}

export class Nid extends Term {
  constructor(readonly nids: string[]) {
    super();
  }

  format() {
    return `NID=${this.nids.join(',')}`;
  }
  toStruct(): StructType {
    return ['nid', this.nids];
  }
}

type TagModes = 'exact' | 'prefix' | 'suffix' | 'infix';
const TAG_MODES: { [key in TagModes]: [string, string] } = {
  exact: ['', ''],
  prefix: ['', '*'],
  suffix: ['*', ''],
  infix: ['*', '*'],
};

export class Tag extends Term {
  constructor(readonly tag: string, readonly mode: TagModes = 'exact') {
    super();
  }

  format() {
    const [before, after] = TAG_MODES[this.mode];
    return `${before}${this.tag}${after}`;
  }
  toStruct(): StructType {
    return ['tag', this.tag, this.mode];
  }
}

type AttrOperators =
  // attr exists
  | '?'
  // case insensitive contains
  | '='
  // substring comparison
  | '=='
  | '==='
  | '^='
  | '^=='
  | '$='
  | '$=='
  | '*='
  | '*=='
  | '+='
  | '+=='
  // string ordering
  | '<='
  | '<=='
  | '>='
  | '>=='
  // number ordering
  | '<<='
  | '>>=';

export class Attr extends Term {
  constructor(readonly name: string, readonly operator: AttrOperators, readonly value: string) {
    super();
  }

  static exists(name: string) {
    return new Attr(name, '?', '');
  }

  format() {
    if (this.operator === '?') {
      return `${this.name}${this.operator}`;
    } else {
      return `${this.name}${this.operator}${escapeString(this.value)}`;
    }
  }
  toStruct(): StructType {
    return ['attr', this.name, this.operator, this.value];
  }
}

export class Text extends Term {
  constructor(readonly value: string) {
    super();
  }

  format() {
    return escapeString(this.value);
  }
  toStruct() {
    return ['text', this.value];
  }
}

export class Not extends Term {
  constructor(readonly condition: Term) {
    super();
  }

  format() {
    return `!(${this.condition.format()})`;
  }
  toStruct(): StructType {
    return ['not', this.condition.toStruct()];
  }
}

function filterTerms(input: any[]): Term[] {
  return input.filter((i) => i instanceof Term && !(i instanceof Nested && i.query === ''));
}

function joinTerms(terms: Term[], separator: string): string {
  const condition = terms.map((term) => term.format()).join(` ${separator} `);
  return `(${condition})`;
}

export class And extends Term {
  readonly conditions: Term[];

  constructor(conditions: any[]) {
    super();
    this.conditions = filterTerms(conditions);
  }

  format() {
    if (this.conditions.length === 0) {
      return new All().format();
    } else {
      return joinTerms(this.conditions, 'AND');
    }
  }
  toStruct(): StructType {
    return ['and', this.conditions.map((term) => term.toStruct())];
  }
}

export class Or extends Term {
  readonly conditions: Term[];

  constructor(conditions: any[]) {
    super();
    this.conditions = filterTerms(conditions);
  }

  format() {
    if (this.conditions.length === 0) {
      return new Term().format();
    } else {
      return joinTerms(this.conditions, 'OR');
    }
  }
  toStruct(): StructType {
    return ['or', this.conditions.map((term) => term.toStruct())];
  }
}

export class Nested extends Term {
  readonly query: string;

  constructor(query: any) {
    super();
    this.query = typeof query === 'string' ? query : '';
  }

  format() {
    return this.query;
  }
  toStruct() {
    return ['nested', this.query];
  }
}
