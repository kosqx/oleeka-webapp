import { storeSettings, deleteSettings, loadSettingsOrNull, findSettings } from 'logic/settings';

export interface AccountAuth {
  username: string;
  accessToken: string;
}

export function getCurrentAccount(): AccountAuth | null {
  return loadSettingsOrNull<AccountAuth>('account');
}

export function getAllAccounts(): AccountAuth[] {
  return Object.values(findSettings<AccountAuth>(/^account-/));
}

export function storeLogin(auth: AccountAuth) {
  storeSettings<AccountAuth>(`account-${auth.username}`, auth);
  storeSettings<AccountAuth>('account', auth);
}

export function deleteCurrentLogin(username: string) {
  deleteSettings(`account-${username}`);
  deleteSettings('account');
}
