interface NamedColor {
  name: string;
  light: string;
  dark: string;
}

export const COLORS: NamedColor[] = [
  { name: 'red', light: '#FFEBEE', dark: '#B71C1C' },
  { name: 'purple', light: '#F3E5F5', dark: '#4A148C' },
  { name: 'blue', light: '#E3F2FD', dark: '#0D47A1' },
  { name: 'cyan', light: '#E0F7FA', dark: '#006064' },
  { name: 'green', light: '#E8F5E9', dark: '#1B5E20' },
  { name: 'yellow', light: '#FFFDE7', dark: '#F57F17' },
  { name: 'orange', light: '#FFF3E0', dark: '#E65100' },
  { name: 'grey', light: '#FAFAFA', dark: '#212121' },
];

export const COLORS_MAPPING: { [U: string]: NamedColor } = Object.assign(
  {},
  ...COLORS.map((x) => ({ [x.name]: x })),
);
