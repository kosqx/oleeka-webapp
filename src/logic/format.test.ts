import { formatDateTime, formatCount, formatFileSize } from 'logic/format';

test.each([
  ['2021-05-05T12:34:56Z', '2021-5-5 14:34:56'],
  ['2021-01-05T12:34:56Z', '2021-1-5 13:34:56'],
])('formatDateTime(%j) === %j', (dt, expected) => {
  expect(formatDateTime(dt)).toEqual(expected);
});

test.each([
  [0, '0'],
  [1, '1'],
  [12, '12'],
  [123, '123'],
  [1234, '1,234'],
  [12345, '12,345'],
  [123456, '123,456'],
  [1234567, '1,234,567'],
  [12345678, '12,345,678'],
  [123456789, '123,456,789'],
])('formatCount(%j) === %j', (value, expected) => {
  expect(formatCount(value)).toEqual(expected);
});

test.each([
  [0, '0 B'],
  [1, '1 B'],
  [12, '12 B'],
  [123, '123 B'],
  [1234, '1.2 KiB'],
  [12345, '12.1 KiB'],
  [123456, '120.6 KiB'],
  [1234567, '1.2 MiB'],
  [12345678, '11.8 MiB'],
  [2 ** 10, '1.0 KiB'],
  [2 ** 20, '1.0 MiB'],
  [2 ** 30, '1.0 GiB'],
  [2 ** 40, '1.0 TiB'],
  [2 ** 50, '1.0 PiB'],
  [2 ** 60, '1.0 EiB'],
  [2 ** 70, '1.0 ZiB'],
])('formatFileSize(%j) === %j', (size, expected) => {
  expect(formatFileSize(size)).toEqual(expected);
});
