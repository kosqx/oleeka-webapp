import { extractLink, convertShareToNotePart } from 'logic/noteEdit';

test.each([
  [null, '', [null, '']],
  ['', '', [null, '']],
  ['https://oleeka.com', '', ['https://oleeka.com', '']],
  ['foo', 'bar', ['foo', 'bar']],
  ['', 'foo bar', [null, 'foo bar']],
  ['', 'http://oleeka.com/bar', ['http://oleeka.com/bar', '']],
  ['', 'foo http://oleeka.com/bar', ['http://oleeka.com/bar', 'foo']],
  ['', 'foo https://oleeka.com/bar', ['https://oleeka.com/bar', 'foo']],
  ['', 'foo\nhttp://oleeka.com/bar', ['http://oleeka.com/bar', 'foo']],
  ['', 'foo\thttp://oleeka.com/bar', ['http://oleeka.com/bar', 'foo']],
  ['', 'foo  http://oleeka.com/bar%20baz', ['http://oleeka.com/bar%20baz', 'foo ']],
  ['', 'http://oleeka.com/bar foo', ['http://oleeka.com/bar', 'foo']],
  ['', 'https://oleeka.com/bar foo', ['https://oleeka.com/bar', 'foo']],
  ['', 'http://oleeka.com/bar\nfoo', ['http://oleeka.com/bar', 'foo']],
  ['', 'http://oleeka.com/bar\tfoo', ['http://oleeka.com/bar', 'foo']],
  ['', 'http://oleeka.com/bar%20baz  foo', ['http://oleeka.com/bar%20baz', ' foo']],
])('extractLink(%j, %j) === %j', (url, text, expected) => {
  expect(extractLink(url, text)).toEqual(expected);
});

test.each([
  [
    { title: null, text: null, url: null },
    { attrs: {}, text: '' },
  ],
  [
    { title: null, text: 'foo', url: null },
    { attrs: {}, text: 'foo' },
  ],
  [
    { title: 'subject', text: 'foo', url: null },
    { attrs: { title: 'subject' }, text: 'foo' },
  ],
  [
    { title: '', text: 'foo', url: 'ftp://oleeka.com' },
    { attrs: { href: 'ftp://oleeka.com', kind: 'bookmark' }, text: 'foo' },
  ],
  [
    { title: '', text: 'foo http://abc', url: '' },
    { attrs: { href: 'http://abc', kind: 'bookmark' }, text: 'foo' },
  ],
  [
    { title: 'title', text: 'foo http://abc', url: '' },
    { attrs: { href: 'http://abc', kind: 'bookmark', title: 'title' }, text: 'foo' },
  ],
])('convertShareToNotePart(%j) === %j', (share, expected) => {
  expect(convertShareToNotePart(share)).toEqual(expected);
});
