import { NoteInputType, NoteAttrsType } from 'data/models';

interface ShareType {
  title: string | null;
  text: string | null;
  url: string | null;
}

interface NotePartType {
  attrs: NoteAttrsType;
  text: string;
}

export function extractLink(url: string | null, text: string): [string | null, string] {
  if (!!url) {
    return [url, text];
  }

  const fullMatch = text.match(/^(https?:\/\/\S+)$/);
  if (fullMatch !== null) {
    return [fullMatch[1], ''];
  }

  const suffixMatch = text.match(/^(.*)\s(https?:\/\/\S+)$/);
  if (suffixMatch !== null) {
    return [suffixMatch[2], suffixMatch[1]];
  }

  const prefixMatch = text.match(/^(https?:\/\/\S+)\s(.*)$/);
  if (prefixMatch !== null) {
    return [prefixMatch[1], prefixMatch[2]];
  }

  return [null, text];
}

export function convertShareToNotePart(share: ShareType): NotePartType {
  const [url, text] = extractLink(share.url, share.text || '');
  const attrs: NoteAttrsType = !!url ? { kind: 'bookmark', href: url } : {};
  if (!!share.title) {
    attrs['title'] = share.title;
  }
  return { attrs, text };
}

export function convertShareToNoteInput(share: ShareType): NoteInputType {
  return {
    nid: null,
    tags: [],
    files: [],
    input: '',
    ...convertShareToNotePart(share),
  };
}
