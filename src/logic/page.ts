type TitlePartType = null | undefined | number | string;

export function setDocumentTitle(...parts: TitlePartType[]) {
  const items = [...parts.filter((i) => !!i || i === 0).map((i) => `${i}`)];
  document.title = [...items, 'oleeka'].join(' — ');
}
