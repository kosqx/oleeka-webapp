import { createStore } from 'redux';

import { loadTheme, storeTheme } from 'logic/settings';
import {
  AccountAuth,
  storeLogin,
  getAllAccounts,
  getCurrentAccount,
  deleteCurrentLogin,
} from 'logic/accounts';

type ThemeOptions = 'light' | 'dark';
interface DialogButton {
  text: string;
  onClick?: () => unknown;
  color?: 'primary' | 'secondary';
  variant?: 'contained' | 'outlined' | 'text';
  autoFocus?: boolean;
}
export interface Dialog {
  title: string;
  text: any;
  buttons: DialogButton[];
}
export interface StoreStateType {
  currentAccount: AccountAuth | null;
  allAccounts: AccountAuth[];
  theme: ThemeOptions;
  dialogs: Dialog[];
}

const initStoreStateType = (): StoreStateType => {
  return {
    theme: loadTheme() as ThemeOptions,
    currentAccount: getCurrentAccount(),
    allAccounts: getAllAccounts(),
    dialogs: [],
  };
};

export type ActionType =
  | {
      type: 'LOGIN';
      payload: AccountAuth;
    }
  | {
      type: 'LOGOUT';
    }
  | {
      type: 'SWITCH_ACCOUNT';
      payload: string;
    }
  | {
      type: 'TOGGLE_THEME';
    }
  | {
      type: 'SHOW_DIALOG';
      payload: Dialog;
    }
  | {
      type: 'HIDE_DIALOG';
    };

export const actions = {
  login(accountAuth: AccountAuth): ActionType {
    return {
      type: 'LOGIN',
      payload: accountAuth,
    };
  },
  logout(): ActionType {
    return { type: 'LOGOUT' };
  },
  switchAccount(username: string): ActionType {
    return { type: 'SWITCH_ACCOUNT', payload: username };
  },
  toggleTheme(): ActionType {
    return { type: 'TOGGLE_THEME' };
  },
  showDialog(dialog: Dialog): ActionType {
    return { type: 'SHOW_DIALOG', payload: dialog };
  },
  hideDialog(): ActionType {
    return { type: 'HIDE_DIALOG' };
  },
};

const allAccountsExcept = (accounts: AccountAuth[], username: string | null): AccountAuth[] =>
  accounts.filter((account) => account.username !== username);

export const mainReducer = (
  state: StoreStateType = initStoreStateType(),
  action: ActionType,
): StoreStateType => {
  switch (action.type) {
    case 'LOGIN':
      const currentAccount = action.payload;
      storeLogin(currentAccount);
      const allAccounts = [
        ...allAccountsExcept(state.allAccounts, currentAccount.username),
        currentAccount,
      ];
      return { ...state, currentAccount, allAccounts };
    case 'LOGOUT':
      if (state.currentAccount !== null) {
        deleteCurrentLogin(state.currentAccount.username);
        const allAccounts = allAccountsExcept(state.allAccounts, state.currentAccount?.username);
        return { ...state, currentAccount: null, allAccounts };
      } else {
        return { ...state, currentAccount: null };
      }
    case 'SWITCH_ACCOUNT':
      const matchingAccounts = state.allAccounts.filter(
        (account) => account.username === action.payload,
      );
      if (matchingAccounts.length === 1) {
        const currentAccount = matchingAccounts[0];
        storeLogin(currentAccount);
        return { ...state, currentAccount };
      } else {
        return state;
      }
    case 'TOGGLE_THEME':
      const newTheme = state.theme === 'light' ? 'dark' : 'light';
      storeTheme(newTheme);
      return { ...state, theme: newTheme };
    case 'SHOW_DIALOG':
      return { ...state, dialogs: [action.payload] };
    case 'HIDE_DIALOG':
      return { ...state, dialogs: [] };
    default:
      return state;
  }
};

export const store = createStore(
  mainReducer,
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__(),
);

export const selectors = {
  getUsername(state: StoreStateType): string | null {
    return state.currentAccount?.username || null;
  },
  getAdditionalAccounts(state: StoreStateType) {
    const currentUsername = state.currentAccount?.username;
    return (state.allAccounts || [])
      .filter((account) => account.username !== currentUsername)
      .sort((a, b) => (a.username < b.username ? -1 : 1));
  },
  getTheme(state: StoreStateType) {
    return state.theme;
  },
  getDialogs(state: StoreStateType) {
    return state.dialogs;
  },
};
