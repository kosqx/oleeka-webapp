export function loadTheme(): string {
  const theme = window.localStorage.getItem('settings-theme');
  if (typeof theme === 'string') {
    return theme;
  }

  const mql = window.matchMedia('(prefers-color-scheme: dark)');
  if (typeof mql.matches === 'boolean') {
    return mql.matches ? 'dark' : 'light';
  }

  return 'light';
}

export function storeTheme(theme: string) {
  window.localStorage.setItem('settings-theme', theme);
}

export function loadSettings<T>(key: string, defaultValue: T): T {
  const value = window.localStorage.getItem(key);
  return value === null ? defaultValue : JSON.parse(value);
}

export function loadSettingsOrNull<T>(key: string): T | null {
  const value = window.localStorage.getItem(key);
  return value === null ? null : JSON.parse(value);
}

export function storeSettings<T>(key: string, value: T): void {
  window.localStorage.setItem(key, JSON.stringify(value));
}

export function deleteSettings(key: string): void {
  window.localStorage.removeItem(key);
}

export function findSettings<T>(pattern: RegExp = /.*/): { [key: string]: T } {
  const result: { [key: string]: T } = {};
  for (var i = 0, len = localStorage.length; i < len; i++) {
    const key = localStorage.key(i);
    if (key !== null && pattern.test(key)) {
      const value = window.localStorage.getItem(key);
      if (value !== null) {
        result[key] = JSON.parse(value);
      }
    }
  }
  return result;
}
