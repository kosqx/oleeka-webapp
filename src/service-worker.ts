/// <reference lib="webworker" />
/* eslint-disable no-restricted-globals */

declare interface PrecacheEntry {
  integrity?: string;
  url: string;
  revision?: string | null;
}
type PrecacheEntries = Array<PrecacheEntry | string>;

declare global {
  interface WorkerGlobalScope {
    __WB_MANIFEST: PrecacheEntries;
  }
}
declare const self: ServiceWorkerGlobalScope;

const CACHE_NAME = 'oleeka-app';
const INDEX_URL_PATTERNS = ['/@', '/add', '/login', '/settings', '/share'];

function shouldReturnIndex(url: string): boolean {
  return INDEX_URL_PATTERNS.some((pattern) => url.indexOf(pattern) >= 0);
}

function urlsFromPrecacheEntries(entries: PrecacheEntries): string[] {
  return entries.map((item) => (typeof item === 'string' ? item : item.url));
}

self.addEventListener('install', (event: ExtendableEvent) => {
  const files = urlsFromPrecacheEntries(self.__WB_MANIFEST);
  files.push('/');
  event.waitUntil(caches.open(CACHE_NAME).then((cache) => cache.addAll(files)));
});

self.addEventListener('fetch', function (event: FetchEvent) {
  if (shouldReturnIndex(event.request.url)) {
    event.respondWith(caches.match(new Request('/index.html')) as Promise<Response>);
  }

  event.respondWith(
    caches.match(event.request).then(function (response) {
      if (response) {
        return response;
      }
      return fetch(event.request);
    }),
  );
});

export {};
