import React from 'react';
import { useSelector } from 'react-redux';

import Link from 'components/Link';
import { selectors } from 'logic/redux';

function IndexView() {
  const currentUser = useSelector(selectors.getUsername);
  // TODO: also detect expired auth
  if (currentUser === null) {
    return <Link redirect to="/login/" />;
  } else {
    return <Link redirect to="/@:username/notes/" />;
  }
}

export default IndexView;
