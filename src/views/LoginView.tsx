import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';

import { Credentials, initCredentials } from 'data/models';
import { apiLogin } from 'data';
import { actions } from 'logic/redux';
import { useRedirect } from 'components/Link';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function LoginView() {
  const classes = useStyles();
  const [credentials, setCredentials] = useState<Credentials>(initCredentials());
  const [errorMessage, setErrorMessage] = useState<string>('');
  const dispatch = useDispatch();
  const redirect = useRedirect();

  const handleSubmit = async (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    setErrorMessage('');
    const response = await apiLogin(credentials);
    if (response.success) {
      const auth = { username: credentials.username, accessToken: response.accessToken };
      dispatch(actions.login(auth));
      redirect({ username: credentials.username, query: '' });
    } else {
      setErrorMessage(response.message);
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            id="email"
            name="email"
            label="Email Address"
            value={credentials.username}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setCredentials({ ...credentials, username: event.target.value })
            }
            error={!!errorMessage}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            autoComplete="email"
            autoFocus
          />
          <TextField
            id="password"
            name="password"
            label="Password"
            type="password"
            value={credentials.password}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setCredentials({ ...credentials, password: event.target.value })
            }
            error={!!errorMessage}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            autoComplete="current-password"
          />
          {!!errorMessage ? (
            <Typography variant="body1" color="error">
              {errorMessage}
            </Typography>
          ) : null}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmit}
          >
            Sign In
          </Button>
        </form>
      </div>
    </Container>
  );
}
