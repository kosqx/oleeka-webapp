import React, { useState } from 'react';
import { useParams } from 'react-router-dom';

import NoteEditWidget from 'components/note/NoteEditWidget';
import { useRedirect } from 'components/Link';
import { saveNote } from 'data';
import { NoteInputType, initNoteInputType } from 'data/models';
import { setDocumentTitle } from 'logic/page';

function NoteAddView() {
  const [noteInput, setNoteInput] = useState<NoteInputType>(initNoteInputType());
  const { username } = useParams<any>();
  const redirect = useRedirect();
  setDocumentTitle('add note');

  const save = async () => {
    const newNote = await saveNote(username, noteInput);
    if (newNote.nid !== null) {
      redirect({ note: newNote.nid });
    }
  };

  return (
    <div className="content-center">
      <NoteEditWidget note={noteInput} onChange={setNoteInput} onSubmit={save} />
    </div>
  );
}

export default NoteAddView;
