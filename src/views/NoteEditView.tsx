import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import NoteEditWidget from 'components/note/NoteEditWidget';
import { useRedirect } from 'components/Link';
import { getNote, saveNote } from 'data';
import { NoteType, NoteInputType, initNoteInputType } from 'data/models';
import {} from 'data/models';
import { setDocumentTitle } from 'logic/page';

function noteToNoteInput(note: NoteType): NoteInputType {
  return {
    nid: note.nid,
    tags: [],
    attrs: note.attrs,
    text: note.text,
    files: note.files,
    input: note.tags.join(' '),
  };
}

function NoteEditView() {
  const [noteInput, setNoteInput] = useState<NoteInputType>(initNoteInputType());
  const { username, nid } = useParams<any>();
  const redirect = useRedirect();
  setDocumentTitle('edit note');

  useEffect(() => {
    const fetchNote = async () => {
      // FIXME: add better handling of "note not found"
      const note = await getNote(username, nid);
      if (note) {
        setNoteInput(noteToNoteInput(note));
      }
      setDocumentTitle(note?.attrs?.title, 'edit note');
    };
    fetchNote();
  }, [username, nid]);

  const save = async () => {
    const newNote = await saveNote(username, noteInput);
    // FIXME: add better handling of "note not saved"
    if (newNote.nid !== null) {
      redirect({ note: newNote.nid });
    }
  };

  return (
    <div className="content-center">
      <NoteEditWidget note={noteInput} onChange={setNoteInput} onSubmit={save} />
    </div>
  );
}

export default NoteEditView;
