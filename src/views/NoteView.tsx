import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';

import NoteShowWidget from 'components/note/NoteShowWidget';
import { getNote } from 'data';
import { NoteType, initNoteType } from 'data/models';
import { setDocumentTitle } from 'logic/page';

function NoteView() {
  const [note, setNote] = useState<NoteType | null>(initNoteType());
  const { username, nid } = useParams<any>();
  setDocumentTitle('note');

  useEffect(() => {
    const fetchNote = async () => {
      const note = await getNote(username, nid);
      setNote(note);
      setDocumentTitle(note?.attrs?.title, 'note');
    };
    fetchNote();
  }, [username, nid]);

  return (
    <div className="content-center">
      {note !== null ? (
        <NoteShowWidget note={note} />
      ) : (
        <Typography variant="h3">Note not found</Typography>
      )}
    </div>
  );
}

export default NoteView;
