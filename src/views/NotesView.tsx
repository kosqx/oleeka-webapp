import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import NoteShowWidget from 'components/note/NoteShowWidget';
import PaginationWidget, { calculatePaginationParams } from 'components/PaginationWidget';
import { getNotes } from 'data';
import { NoteType, NotesResponseType, initNotesResponseType } from 'data/models';
import { Nested } from 'data/queryTerms';
import { setDocumentTitle } from 'logic/page';

const PER_PAGE = 10;

function NotesDateSection(props: { dt: string }) {
  const date = new Date(props.dt);
  const options = {
    weekday: 'long',
    year: 'numeric',
    month: '2-digit',
    day: 'numeric',
  };
  return <h1>{date.toLocaleDateString('default', options)}</h1>;
}

function isTheSameDay(date1: string, date2: string): boolean {
  const dt1 = new Date(date1);
  const dt2 = new Date(date2);
  return (
    dt1.getFullYear() === dt2.getFullYear() &&
    dt1.getMonth() === dt2.getMonth() &&
    dt1.getDate() === dt2.getDate()
  );
}

function buildItems(notes: NoteType[]): React.ReactElement[] {
  const result: React.ReactElement[] = [];
  let previousCreated: string = '1000-01-01T00:00:00Z';

  for (var note of notes) {
    if (!isTheSameDay(note.attrs.created, previousCreated)) {
      result.push(<NotesDateSection key={note.attrs.created} dt={note.attrs.created} />);
    }
    previousCreated = note.attrs.created;
    result.push(<NoteShowWidget key={note.nid} note={note} />);
  }
  return result;
}

function NotesView() {
  const [notes, setNotes] = useState<NotesResponseType>(initNotesResponseType());
  const [page, setPage] = useState<number>(0);
  const { username, query } = useParams<any>();
  setDocumentTitle(query, 'notes');

  useEffect(() => {
    setPage(0);
  }, [query]);
  useEffect(() => {
    const fetchNotes = async () => {
      setNotes(
        await getNotes({
          username: username,
          query: new Nested(query),
          fetchTotal: true,
          ...calculatePaginationParams(page, PER_PAGE),
        }),
      );
    };
    fetchNotes();
  }, [username, query, page]);

  return (
    <div className="content-center">
      {buildItems(notes.notes)}
      <PaginationWidget page={page} parPage={PER_PAGE} total={notes.total} onChange={setPage} />
    </div>
  );
}

export default NotesView;
