import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import Link from 'components/Link';
import PaginationWidget, { calculatePaginationParams } from 'components/PaginationWidget';
import { getNotes } from 'data';
import { NoteType, NotesResponseType, initNotesResponseType } from 'data/models';
import { Attr, And, Nested } from 'data/queryTerms';
import { setDocumentTitle } from 'logic/page';

const PER_PAGE = 24;

function PhotoFrame(props: { note: NoteType }) {
  const note = props.note;

  return (
    <span style={{ width: 480, height: 480, display: 'inline-block', border: '1px grey solid' }}>
      <img
        src={'data:image/jpeg;base64,' + note.files[0].data.join('')}
        width={((note.files[0].meta as { image_width: number }).image_width || 960) / 2}
        alt={note.files[0].name}
      />
      <br />
      <Link note={note.nid || ''}>{note.files[0].name}</Link>
    </span>
  );
}

function PhotosView() {
  const [notes, setNotes] = useState<NotesResponseType>(initNotesResponseType());
  const [page, setPage] = useState<number>(0);
  const { username, query } = useParams<any>();
  setDocumentTitle(query, 'photos');

  useEffect(() => {
    setPage(0);
  }, [query]);
  useEffect(() => {
    const fetchNotes = async () => {
      setNotes(
        await getNotes({
          username,
          query: new And([new Attr('kind', '===', 'photo'), !!query ? new Nested(query) : null]),
          selectFields: 'files,files_data',
          fetchTotal: true,
          ...calculatePaginationParams(page, PER_PAGE),
        }),
      );
    };
    fetchNotes();
  }, [username, query, page]);

  return (
    <div className="notes-photo">
      {notes.notes.map((note: NoteType) => (
        <PhotoFrame key={note.nid} note={note} />
      ))}
      <PaginationWidget page={page} parPage={PER_PAGE} total={notes.total} onChange={setPage} />
    </div>
  );
}

export default PhotosView;
