import React from 'react';
import { useParams } from 'react-router-dom';

import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import Brightness4Icon from '@material-ui/icons/Brightness4';
import InfoIcon from '@material-ui/icons/Info';
import DeveloperModeIcon from '@material-ui/icons/DeveloperMode';

import AboutPage from 'components/settings-pages/AboutPage';
import DebugPage from 'components/settings-pages/DebugPage';
import ThemePage from 'components/settings-pages/ThemePage';

import ListWidget, { ListSections } from 'components/ListWidget';
import { setDocumentTitle } from 'logic/page';

const THEME_TO_COMPONENT: { [page: string]: React.ComponentType } = {
  about: AboutPage,
  debug: DebugPage,
  theme: ThemePage,
};

function SettingsList(props: { selectedPage: string }) {
  const sections: ListSections = [
    {
      id: 'interface',
      items: [
        {
          id: 'theme',
          primaryText: 'Theme',
          secondaryText: 'Dark mode, font size',
          icon: Brightness4Icon,
          linkTo: '/settings/theme',
        },
      ],
    },
    {
      id: 'meta',
      items: [
        {
          id: 'about',
          primaryText: 'About Oleeka',
          secondaryText: 'Version, help, licence',
          icon: InfoIcon,
          linkTo: '/settings/about',
        },
        {
          id: 'debug',
          primaryText: 'Debug',
          secondaryText: 'Only for developers',
          icon: DeveloperModeIcon,
          linkTo: '/settings/debug',
        },
      ],
    },
  ];
  return <ListWidget selectedItem={props.selectedPage} sections={sections} />;
}

function SettingsView() {
  const { page } = useParams<any>();
  setDocumentTitle(page, 'settings');

  const PageComponent = THEME_TO_COMPONENT[page];

  return (
    <div className="content-center">
      <Grid container spacing={2}>
        <Hidden {...(!!page ? { smDown: true } : {})}>
          <Grid item xs={12} md={4}>
            <Paper>
              <SettingsList selectedPage={page} />
            </Paper>
          </Grid>
        </Hidden>
        <Grid item xs={12} md={8}>
          <Paper style={{ padding: 8 }}>
            {!!page ? <PageComponent /> : <Typography variant="h3">Select setting</Typography>}
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

export default SettingsView;
