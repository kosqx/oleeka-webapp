import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import NoteEditWidget from 'components/note/NoteEditWidget';
import NoteShowWidget from 'components/note/NoteShowWidget';
import { saveNote } from 'data';
import { NoteType, NoteInputType, initNoteInputType } from 'data/models';
import { convertShareToNoteInput } from 'logic/noteEdit';
import { setDocumentTitle } from 'logic/page';

function ShareView() {
  const [note, setNote] = useState<NoteType | null>(null);
  const [noteInput, setNoteInput] = useState<NoteInputType>(initNoteInputType());
  const { username } = useParams<any>();
  setDocumentTitle('share');

  useEffect(() => {
    const searchParams = new URL((window.location as unknown) as string).searchParams;
    setNoteInput(
      convertShareToNoteInput({
        title: searchParams.get('title'),
        text: searchParams.get('text'),
        url: searchParams.get('url'),
      }),
    );
  }, []);

  const save = async () => {
    const newNote = await saveNote(username, noteInput);
    setNote(newNote);
  };

  return (
    <div className="content-center">
      {note !== null ? (
        <NoteShowWidget note={note} />
      ) : (
        <NoteEditWidget note={noteInput} onChange={setNoteInput} onSubmit={save} />
      )}
    </div>
  );
}

export default ShareView;
