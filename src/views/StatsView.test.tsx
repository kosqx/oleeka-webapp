import { WeekdayHourCounts } from 'views/StatsView';

test('WeekdayHourCounts works', () => {
  const counts = new WeekdayHourCounts().build({
    '2021-11-17T10': 1,
    '2021-11-17T12': 10,
    '2021-11-18T10': 100,
  });
  // assumes Europe/Warsaw time zone
  expect(counts.get(2, 11)).toBe(1);
  expect(counts.get(2, 13)).toBe(10);
  expect(counts.get(3, 11)).toBe(100);
  // weekday summary
  expect(counts.get(2, 24)).toBe(11);
  expect(counts.get(3, 24)).toBe(100);
  // hour summary
  expect(counts.get(7, 11)).toBe(101);
  expect(counts.get(7, 13)).toBe(10);
  // total summary
  expect(counts.get(7, 24)).toBe(111);
});
