import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import { getRelated } from 'data';
import { RelatedType, initRelatedType } from 'data/models';
import { Nested } from 'data/queryTerms';
import { formatCount } from 'logic/format';
import { setDocumentTitle } from 'logic/page';

const useStyles = makeStyles({
  table: {
    '& td': {
      textAlign: 'right',
    },
    '& th': {
      textAlign: 'right',
      fontWeight: 'bold',
    },
  },
});

const WEEKDAYS = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
  'Total',
];

export class WeekdayHourCounts {
  /// assumes that Monday = 0, Tuesday = 1, ..., Sunday = 6
  counts: number[];

  constructor() {
    this.counts = Array((24 + 1) * (7 + 1)).fill(0);
  }

  increment(weekday: number, hour: number, value: number): void {
    this.counts[weekday * 25 + hour] += value;
    this.counts[weekday * 25 + 24] += value;
    this.counts[7 * 25 + hour] += value;
    this.counts[7 * 25 + 24] += value;
  }

  build(dates: { [attr: string]: number } | null) {
    for (const [key, value] of Object.entries(dates || {})) {
      const dt = new Date(key + ':00:00Z');
      this.increment((dt.getDay() + 6) % 7, dt.getHours(), value);
    }
    return this;
  }

  get(weekday: number, hour: number): number {
    return this.counts[weekday * 25 + hour];
  }
}

function formatHour(hour: number): string {
  return hour < 10 ? `0${hour}:00` : `${hour}:00`;
}

function PunchCardTable(props: { dates: WeekdayHourCounts }) {
  const classes = useStyles();

  return (
    <TableContainer component={Paper} className={classes.table}>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Hour</TableCell>
            {WEEKDAYS.map((weekday) => (
              <TableCell>{weekday}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {Array(24)
            .fill(0)
            .map((_, hour) => (
              <TableRow key={hour}>
                <TableCell component="th">{formatHour(hour)}</TableCell>
                {WEEKDAYS.map((_, weekdayIdx) => (
                  <TableCell scope="row">
                    {formatCount(props.dates.get(weekdayIdx, hour))}
                  </TableCell>
                ))}
              </TableRow>
            ))}
        </TableBody>
        <TableHead component="tfoot">
          <TableRow>
            <TableCell>Total</TableCell>
            {WEEKDAYS.map((_, weekdayIdx) => (
              <TableCell component="th">{formatCount(props.dates.get(weekdayIdx, 24))}</TableCell>
            ))}
          </TableRow>
        </TableHead>
      </Table>
    </TableContainer>
  );
}

function StatsView() {
  const [related, setRelated] = useState<RelatedType>(initRelatedType());
  const { username, query } = useParams<any>();
  setDocumentTitle(query, 'stats');

  useEffect(() => {
    const fetchRelated = async () => {
      setRelated(
        await getRelated({
          username,
          query: new Nested(query),
          fetchTags: false,
          fetchDates: 'hour',
        }),
      );
    };
    fetchRelated();
  }, [username, query]);

  return (
    <div className="content-center">
      <Typography variant="h2" gutterBottom>
        Stats
      </Typography>
      <PunchCardTable dates={new WeekdayHourCounts().build(related.dates)} />
    </div>
  );
}

export default StatsView;
