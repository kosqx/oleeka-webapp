import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import Link from 'components/Link';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import { getRelated } from 'data';
import { RelatedType, initRelatedType } from 'data/models';
import { Nested } from 'data/queryTerms';
import { formatCount } from 'logic/format';
import { setDocumentTitle } from 'logic/page';

const useStyles = makeStyles({
  table: {
    '& a': {
      color: 'inherit',
    },
  },
  countColumn: {
    width: 120,
  },
});

type TagCount = { tag: string; count: number };

function TagCountTable(props: { tags: TagCount[] }) {
  const classes = useStyles();

  return (
    <TableContainer component={Paper} className={classes.table}>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell align="right" className={classes.countColumn}>
              Count
            </TableCell>
            <TableCell>Tag</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.tags.map((row: TagCount) => (
            <TableRow key={row.tag}>
              <TableCell align="right">{formatCount(row.count)}</TableCell>
              <TableCell component="th" scope="row">
                <Link query={row.tag}>{row.tag}</Link>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

function TagsView() {
  const [related, setRelated] = useState<RelatedType>(initRelatedType());
  const { username, query } = useParams<any>();
  setDocumentTitle(query, 'tags');

  useEffect(() => {
    const fetchNotes = async () => {
      setRelated(
        await getRelated({
          username,
          query: new Nested(query),
          fetchTags: true,
        }),
      );
    };
    fetchNotes();
  }, [username, query]);

  const tags: TagCount[] = Object.entries(related.tags || {})
    .map(([key, value]) => {
      return { tag: key, count: value };
    })
    .sort((a, b) => b.count - a.count);

  return (
    <div className="content-center">
      <Typography variant="h2" gutterBottom>
        {!query || query === 'ALL' ? 'All tags' : `Related tags for query "${query}"`}
      </Typography>
      <TagCountTable tags={tags} />
    </div>
  );
}

export default TagsView;
